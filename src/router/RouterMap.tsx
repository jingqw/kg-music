import * as React from "react";
import Loadable from "react-loadable";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import App from "../App";
import loading from "./loading";

// 路由懒加载
const page = (viewpath: string) => import(`../view${viewpath}`);

const RouterList: any[] = [
  {
    path: "/",
    component: page("/Home/Home"),
  },
  {
    path: "/home",
    component: () => page("/Home/Home"),
  },
  {
    path: "/song",
    component: () => page("/Song/Index"),
  },
];
const RouterMap = () => (
  <Router>
    <App>
    <Switch>
      {RouterList.map((item) => (
        <Route
          key={item.path}
          exact={true}
          path={item.path}
          component={Loadable({
            loader: item.component,
            loading,
          })}
        />
      ))}
    </Switch>
    </App>
  </Router>
);

export default RouterMap;
