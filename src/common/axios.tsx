import { interceptors } from './interceptors';

class axiosHttp {
  axios: any;
  constructor() {
    // 赋值axios实例
    this.axios = new interceptors().getInterceptors();
  }

  /**
    get方法请求
    @param url  接口地址
    @param data  接口参数
    @param headers  响应头
    @param params  其余参数
  */

  get({ url = '', data = {}, headers = {}, params = {} }) {
    return new Promise((resolve, reject) => {
      this.axios({ method: "GET", url, params: data, headers, ...params })
        .then((res: any) => {
          resolve(res)
        }).catch((e: object) => {
          reject(e)
        });
    })
  }

  // 错误码处理
  private errorHandle(res: any) {
  }

}

export const $api = new axiosHttp();
