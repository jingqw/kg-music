import axios from 'axios';

export class interceptors {
  instance: any;

  constructor() {
    // create axios instance
    this.instance = axios.create({
      // baseURL: 'http://mobilecdnbj.kugou.com/api/v3', // 酷狗
      baseURL: 'http://localhost:3000/', // 解决跨域 可以在package.json中加 "proxy": "http://mobilecdnbj.kugou.com/"
      timeout: 60000
    });
    // init interceptors
    this.initInterceptors();
  }
  // 外部使用axios实例
  getInterceptors() {
    return this.instance;
  }

  // init interceptors
  initInterceptors() {
    // 设置post请求头
    // this.instance.defaults.headers.post['Content-Type'] = 'application/json; charset=UTF-8;'; // application/x-www-form-urlencoded
    // this.instance.defaults.headers.post['Accept-Language'] = 'zh-cn,zh;';

    //请求拦截
    this.instance.interceptors.request.use((config: object) => {
      // token判断等处理

      return config;
    }, (error: object) => {
      // console.log('请求出错,测试下面的return promise.reject0');
      return Promise.reject(error);
    });

    //响应拦截
    this.instance.interceptors.response.use((res: any) => {
      const status = res.status;
      if (status === 200) {
        // status = 200请求成功
        if(res.data.code === 200){
          return Promise.resolve(res.data);
        }
      } else {
        // 请求失败

      }
    }, (error: object) => {
      this.errorHandle(error);
      // console.log(error);
      return Promise.reject(error);
    });
  }

  // 错误码处理
  private errorHandle(res: any) {
  }

}
