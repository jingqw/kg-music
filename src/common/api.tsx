import { $api } from "./axios";

// 获取banner
export const getBanner = () => {
  return $api.get({ url: "/banner" });
};
// 获取推荐歌单
export const getHotList = (data = { limit: 6 }) => {
  return $api.get({ url: "/personalized", data });
};
// 获取推荐新歌
export const getHotSong = (data = { limit: 10 }) => {
  return $api.get({ url: "/personalized/newsong", data });
};

// 获取歌曲url
export const geSongUrl = (data = { id: 0 }) => {
  return $api.get({ url: "/music/url", data });
};
