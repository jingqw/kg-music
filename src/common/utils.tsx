const utils = {
  formateBigNum(num: number) {
    if (isNaN(num)) {
      return "--";
    }
    let result: number | string = 0;
    if (num <= 99999) {
      result = num;
    }
    if (num > 99999 && num <= 99999999) {
      result = Math.floor(num / 100) / 100 + "万";
    }
    if (num >= 100000000) {
      result = (num / 100000000).toFixed(2) + "亿";
    }
    return result;
  },
};

export default utils;
