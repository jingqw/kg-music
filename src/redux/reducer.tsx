const defaultState = {
  songDetail: {}
};

export default (state = defaultState, action:any) => {
  if(action.type === 'set_song_detail'){
    let newState = JSON.parse(JSON.stringify(state));
    newState.songDetail = action.songDetail;
    return newState;
  }
  return state
};
