import React from "react";
import { connect } from "react-redux";

// 哪些 Redux 全局的 state 是我们组件想要通过 props 获取的
const mapStateToProps = (state: any) => {
  return {  };
};

// 哪些 action 创建函数是我们想要通过 props 获取的
const mapDispatchToProps = (dispatch: any) => {
  return {
    // setSongDetail(song: object) {
    //   dispatch({
    //     type: "set_song_url",
    //     songDetail: song,
    //   });
    // },
  };
};

const Sound = () => {
  // ！！！ 要将audio当作全局组件使用
  return (
    <audio
      src="http://m10.music.126.net/20200424115653/54116c1534bf2722d913267ea5d4b93f/ymusic/5de2/264f/375e/a35a0f142b350d125b95629c82f353b4.mp3"
      controls
      preload=""
      className="global-audio"
      id="globalAudio"
      // style={{
      //   position: "fixed",
      //   top: "-100px",
      //   left: "-100px",
      // }}
    ></audio>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Sound);
