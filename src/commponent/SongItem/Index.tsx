import React from "react";
import "./Index.less";

const SongItem = (props: any) => {
  // 创建播放按钮element
  // const playDom = React.createElement("img", {
  //   style: {
  //     width: "0.26rem",
  //     height: "0.26rem",
  //   },
  //   src: playIcon,
  // });
  const {songData} = props;
  return (
    <div className="song-item">
      <span className="add-next-btn">+</span>
      <img src={songData.album.picUrl} alt="" className="song-img" />
      <div>
        <p className="song-name">{songData.name}</p>
        <p className="text-ellipsis song-author-wrap">
          <span className="song-author">{songData.artists[0].name}</span>
          <span>{songData.alias.length ? songData.alias[0] : ""}</span>
        </p>
      </div>
      <img src="../../assets/play.png" alt="" className="play-img" />
    </div>
  );
};

export default SongItem;
