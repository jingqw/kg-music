import React from "react";
import "./Index.less";

const Navigation = (props: any) => {
  const navMenu = [
    {
      name: "我的",
      id: 0,
    },
    {
      name: "发现",
      id: 1,
    },
  ];
  const { currentSwiper } = props;
  return (
    <section className="nav-container">
      <div className="nav-mask"></div>
      <div className="nav-wrap">
        <div>菜单</div>
        <ul className="nav-menu">
          {navMenu.map((item: any) => {
            return (
              <li
                key={item.id}
                className={`nav-menu-item ${
                  currentSwiper === item.id ? "active" : ""
                }`}
              >
                {item.name}
              </li>
            );
          })}
        </ul>
        <div>搜索</div>
      </div>
    </section>
  );
};
export default Navigation;
