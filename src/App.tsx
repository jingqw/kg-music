import React from "react";
import Sound from "./commponent/sound/sound";
export default (props: any) => {
  return (
    <section className="App">
      <Sound />
      {props.children}
    </section>
  );
};
