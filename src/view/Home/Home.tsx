import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import Navigation from "../../commponent/navigation/Index";
import Banner from "../Index/banner/Banner";
import Index from "../Index/Index";

import Swiper from "swiper";
import "swiper/css/swiper.css";

export default withRouter((props: any) => {
  const [swiperIndex, setSwiperIndex] = useState(1);

  useEffect(() => {
    // 初始化swiper，监听切换并赋值给顶部的导航组件
    new Swiper("#homePage", {
      initialSlide: 1,
      on: {
        slideChange() {
          const _self: any = this;
          setSwiperIndex(_self.activeIndex);
        },
      },
    });
  }, []);

  return (
    <div>
      {/* 首页才展示的顶部导航 */}
      <Navigation currentSwiper={swiperIndex} />

      {/* swiper容器 */}
      <div className="swiper-container" id="homePage">
        <div className="swiper-wrapper">
          <div className="swiper-slide">我的页面</div>
          <div className="swiper-slide">
            <Banner />
            <Index />
          </div>
        </div>
      </div>
    </div>
  );
});
