import React, { useState, useEffect } from "react";

import { getBanner } from "../../../common/api";

import Swiper from "swiper";
// import "swiper/css/swiper.css"; Home中导入了  这里可以不导入
import './banner.less'

const Banner = (props: any) => {
  const [banners, setBanners] = useState([]);

  useEffect(() => {
    new Swiper("#bannerWrap", {
      nested: true,
      autoplay: true,
      pagination: {
        el: ".swiper-pagination-banner",
      },
    });
    getBanner().then((res: any) => {
      setBanners(res.banners);
    });
  }, []);

  // 样式
  const bannerStyle: React.CSSProperties = {
    overflow: "hidden",
    borderRadius: "0.05rem",
    margin: "0.05rem 0.12rem 0.16rem 0.12rem",
  };

  return (
    <div className="banner-wrap" style={bannerStyle}>
      <div className="swiper-container" id="bannerWrap">
        <div className="swiper-wrapper">
          {banners.map((item: any, index: number) => (
            <div key={index} className="swiper-slide">
              <img
                src={item.picUrl}
                alt=""
                key={item.targetId}
                style={{ width: "100%", verticalAlign: "top" }}
              />
            </div>
          ))}
        </div>
        <div className="swiper-pagination swiper-pagination-banner"></div>
      </div>
    </div>
  );
};

export default Banner;
