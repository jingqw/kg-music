import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";

import { connect } from "react-redux";

import SongItem from "../../../commponent/SongItem/Index";

import { getHotSong } from "../../../common/api";
import "./Index.less";

// 哪些 Redux 全局的 state 是我们组件想要通过 props 获取的
const mapStateToProps = (state: any) => {
  return { songDetail: state.songDetail };
};

// 哪些 action 创建函数是我们想要通过 props 获取的
const mapDispatchToProps = (dispatch: any) => {
  return {
    setSongDetail(detail: any) {
      dispatch({
        type: "set_song_detail",
        songDetail: detail,
      });
    },
  };
};

const NewSong = () => {
  const [newSongs, setNewSongs] = useState([]);

  useEffect(() => {
    getHotSong().then((res: any) => {
      setNewSongs(res.result.map((item: any) => item.song));
    });
  }, []);

  return (
    <div className="new-song-wrap">
      <div className="title">推荐新音乐</div>
      <div className="song-ul">
        {newSongs.map((item: any) => {
          return <SongItem key={item.id} songData={item} />;
        })}
      </div>
    </div>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NewSong));
