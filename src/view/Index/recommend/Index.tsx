import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";

import { getHotList } from "../../../common/api";
import "./Index.less";

import utils from "../../../common/utils";

export default withRouter(() => {
  const [hotList, setHotList] = useState([]);

  useEffect(() => {
    getHotList().then((res: any) => {
      setHotList(res.result);
    });
  }, []);

  // const changeTheme = () => {};

  return (
    <div className="recommend-wrap" style={{ padding: "0 0.12rem" }}>
      <div className="title">推荐歌单</div>
      <div className="hot-wrap">
        {hotList.map((item:any) => {
          return (
            <div className="hot-item" key={item.id}>
              <span className="play-count">
                {utils.formateBigNum(item.playCount)}
              </span>
              <img src={item.picUrl} alt="" className="hot-img" />
              <p className="text-ellipsis hot-desc">{item.name}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
});
