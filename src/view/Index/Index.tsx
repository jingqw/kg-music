import React from "react";

import Recommend from "./recommend/Index";
import NewSong from "./newSong/Index";

export default () => {
  return (
    <div className="Index-wrap">
      <Recommend />
      <NewSong />
    </div>
  );
};
