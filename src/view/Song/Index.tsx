import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";

import "./Index.less";

const Song = (props: any) => {
  useEffect(() => {}, []);

  // ！！！ 要将audio当作全局组件使用，因为离开此页面，歌曲还要播放呢 ！！！ 

  return (
    <section className="song-detail-wrap"> 
      {/* 歌曲名 */}
      <div className="song-name-wrap">
        <p className="song-name">告白气球</p>
        <p className="singer">周杰伦</p>
      </div>

      {/* 满屏歌词/专辑图/半屏歌词 的视图切换*/}
      <div className="song-lyric-wrap">
        <p className="lyric-item">塞纳河畔 左岸的咖啡</p>
        <p className="lyric-item">我手一杯 品尝你的美</p>
      </div>

      <div className="song-control-wrap">
        <p className="area area-1">
          <span>收藏</span>
          <span>下载</span>
          <span>评论</span>
          <span>更多</span>
        </p>
        <div className="area area-2">
          <span>00:00</span>
          <p className="play-bar">
            <span className="played-line"></span>
            <span className="play-point"></span>
          </p>
          <span>4:16</span>
        </div>
        <p className="area area-3">
          <span>循环</span>
          <span>上一首</span>
          <span>播放</span>
          <span>下一首</span>
          <span>播放列表</span>
        </p>
      </div>
    </section>
  );
};

export default withRouter(Song);
