const { override, fixBabelImports, addLessLoader } = require('customize-cra');
const theme = require('./src/theme.json');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd-mobile',
    // libraryDirectory: 'es',
    style: true, //# 这里不注释掉就无法使用less修改主题，这里的功能是样式按需加载
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: theme
  })
);